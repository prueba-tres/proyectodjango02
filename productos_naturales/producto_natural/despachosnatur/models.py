from django.db import models

# Create your models here.
class despachos(models.Model):
    numero_de_despacho=models.CharField(max_length=60)
    nombre_cliente=models.CharField(max_length=30)
    direccion=models.CharField(max_length=60)
    telefono=models.CharField(max_length=15)
    productos=models.CharField(max_length=30)
    peso_en_kg=models.IntegerField()
    medidas=models.CharField(max_length=10)
    fecha_de_igreso=models.CharField(max_length=10)
    fecha_de_envio=models.CharField(max_length=10)
    estado=models.CharField(max_length=9)
    
    def __str__(self):
        return self.nombre_cliente