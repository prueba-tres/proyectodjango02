from django.shortcuts import render
from despachosnatur.models import despachos
from django.http import HttpResponse
# Create your views here.
def index(request):
    return render(request,"index.html")

def registrar_despacho(request):
    return render(request,"registrar_despacho.html")

def reg_despacho(request):
    numero_de_despacho=request.GET["txt_numero_de_despacho"]
    nombre_cliente=request.GET["txt_nombre_cliente"]
    direccion=request.GET["txt_direccion"]
    telefono=request.GET["txt_telefono"]
    productos=request.GET["txt_productos"]
    peso_en_kg=request.GET["txt_peso_en_kg"]
    medidas=request.GET["txt_medidas"]
    fecha_de_igreso=request.GET["txt_fecha_de_igreso"]
    fecha_de_envio=request.GET["txt_fecha_de_envio"]
    estado=request.GET["txt_estado"]
    if len(numero_de_despacho)>0 and len(nombre_cliente)>0 and len(direccion)>0 and len(telefono)>0 and len(productos)>0 and len(peso_en_kg)>0 and len(medidas)>0 and len(fecha_de_igreso)>0 and len(fecha_de_envio)>0 and len(estado)>0:
        desp=despachos(numero_de_despacho=numero_de_despacho,nombre_cliente=nombre_cliente,direccion=direccion,telefono=telefono,productos=productos,peso_en_kg=peso_en_kg,medidas=medidas,fecha_de_igreso=fecha_de_igreso,fecha_de_envio=fecha_de_envio,estado=estado)  
        desp.save()
        alerta="Despacho ingresado..."
    else:
        alerta="Despacho no ingresado..."
    return HttpResponse(alerta)
